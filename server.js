const express = require('express');

const app = express();

const { findQuoteInDatabase } = require('./databaseQuery.js');
const { addShipmentToDatabase } = require('./databaseQuery.js');
const { getShipmentFromDatabase } = require('./databaseQuery.js');
const { deleteShipmentFromDatabase } = require('./databaseQuery.js');

app.use(express.json());

app.use((request, response, next) => {
  response.setHeader('Access-Control-Allow-Origin', '*');
  response.setHeader('Access-Control-Allow-Methods', 'GET,HEAD,OPTIONS,POST,PUT');
  response.setHeader('Access-Control-Allow-Headers', 'Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers');

  next();
});

/* -----------------------------Create "rate" collection----------------------------------*/

// MongoClient.connect(url, function(err, db) {
//   if (err) throw err;
//   console.log("Database created!");
//   db.close();
// });

// MongoClient.connect(url, function(err, db) {
//     if (err) throw err;
//     var dbo = db.db("mydb");
//     dbo.createCollection("rate", function(err, res) {
//       if (err) throw err;
//       console.log("Collection created!");
//       db.close();
//       });
//     });


/* -----------------------------Fill "rate" collection----------------------------------*/
// MongoClient.connect(url, function(err, db) {
//     if (err) throw err;
//     var dbo = db.db("mydb");
//     var myobj = [
//         { weight: 250, price: 12.43, from: "FR", to: "FR"},
//         { weight: 500, price: 12.43, from: "FR", to: "FR"},
//         { weight: 750, price: 15.42, from: "FR", to: "FR"},
//         { weight: 1000, price: 15.42, from: "FR", to: "FR"},
//         { weight: 2000, price: 20.77, from: "FR", to: "FR"},
//         { weight: 3000, price: 26.07, from: "FR", to: "FR"},
//         { weight: 4000, price: 31.43, from: "FR", to: "FR"},
//         { weight: 5000, price: 36.77, from: "FR", to: "FR"},
//         { weight: 6000, price: 42.13, from: "FR", to: "FR"},
//         { weight: 7000, price: 47.49, from: "FR", to: "FR"},
//         { weight: 8000, price: 52.83, from: "FR", to: "FR"},
//         { weight: 9000, price: 58.83, from: "FR", to: "FR"},
//         { weight: 10000, price: 63.54, from: "FR", to: "FR"},
//         { weight: 11000, price: 88.19, from: "FR", to: "FR"},
//         { weight: 12000, price: 88.19, from: "FR", to: "FR"},
//         { weight: 13000, price: 88.19, from: "FR", to: "FR"},
//         { weight: 14000, price: 88.19, from: "FR", to: "FR"},
//         { weight: 15000, price: 88.19, from: "FR", to: "FR"},
//         { weight: 16000, price: 100, from: "FR", to: "FR"},
//       ];

//     dbo.collection("rate").insertMany(myobj, function(err, res) {
//         if (err) throw err;
//         console.log("Number of documents inserted: " + res.insertedCount);
//         db.close();
//         });
//     });


/* -----------------------------Get quote----------------------------------*/
app.post('/client/getquote', (request, response) => {
  const weightInGram = request.body.weight * 1000;
  const quoteID = request.body.id + 1;
  const origin = request.body.sender_country;
  const dest = request.body.receiver_country;
  if (weightInGram <= 0) throw new Error('Invalid weight!');
  findQuoteInDatabase(quoteID, weightInGram, origin, dest)
    .then((r) => {
      response.send(r);
    })
    .catch((err) => {
      if (err) throw err;
    });
});


/* -----------------------------Create shipment collection----------------------------------*/
// MongoClient.connect(url, function(err, db) {
//     if (err) throw err;
//     var dbo = db.db("mydb");
//     dbo.createCollection("shipment", function(err, res) {
//       if (err) throw err;
//       console.log("Collection created!");
//       db.close();
//       });
//     });

/* -----------------------------Create shipment----------------------------------*/
app.post('/client/createshipment', (request, response) => {
  const shipmentData = request.body;
  addShipmentToDatabase(shipmentData)
    .then((r) => {
      response.send(r);
    })
    .catch((err) => {
      if (err) throw err;
    });
});


/* -----------------------------Get shipment----------------------------------*/
app.get('/client/getshipment/ref/:ref_num', (request, response) => {
  const refNum = Number(request.params.ref_num);
  getShipmentFromDatabase(refNum)
    .then((r) => {
      response.send(r);
    })
    .catch((err) => {
      if (err) response.send({ data: { ref: '' } });
    });
});


/* -----------------------------Delete shipment----------------------------------*/
app.post('/client/deleteshipment', (request, response) => {
  const refNum = request.body.ref;
  deleteShipmentFromDatabase(refNum)
    .then((r) => {
      response.send(r);
    })
    .catch((err) => {
      if (err) response.send({ data: [{ status: 'NOK', message: 'shipment cannot be deleted' }] });
    });
});


/* -----------------------------Open port----------------------------------*/
app.listen(1234, () => {});
