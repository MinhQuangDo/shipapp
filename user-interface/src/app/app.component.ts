import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  constructor(private http: HttpClient) { }
  sender_name: string;
  sender_email: string;
  sender_phone: number;
  sender_address: string;
  sender_locality: string;
  sender_postal_code: number;
  sender_country: string;

  receiver_name: string;
  receiver_email: string;
  receiver_phone: number;
  receiver_address: string;
  receiver_locality: string;
  receiver_postal_code: number;
  receiver_country: string;

  length: number;
  width: number;
  height: number;
  weight: number;

  //additional paramters
  price: number;
  id: number = 0;
  have_quote: boolean = false;
  return_ref_num: number;

  getQuote() {
    this.http.post('http://localhost:1234/client/getquote', {id: this.id, weight: this.weight, sender_country: this.sender_country, receiver_country: this.receiver_country}).subscribe( (response : any) => { 
      this.id += 1;
      if (response != null && response.data !== undefined)
      {
        this.price = response.data[0].price;
        this.have_quote = true;
      }
    });
  }

  createShipment() {
      let shipment_info = {
        data: {
          quote: {
            id: this.id
          },
          origin: {
            contact: {
              name: this.sender_name,
              email: this.sender_email,
              phone: this.sender_phone
            },
            address: {
              country_code: this.sender_country,
              locality: this.sender_locality,
              postal_code: this.sender_postal_code,
              address_line1: this.sender_address
            }
          },
          destination: {
            contact: {
              name: this.receiver_name,
              email: this.receiver_email,
              phone: this.receiver_phone
            },
            address: {
              country_code: this.receiver_country,
              locality: this.receiver_locality,
              postal_code: this.receiver_postal_code,
              address_line1: this.receiver_address
            }
          },
          package: {
            dimensions: {
              height: this.height,
              width: this.width,
              length: this.length,
              unit: "cm"
            },
            grossWeight: {
              amount: this.weight,
              unit: "kg"
            }
          },
          cost: this.price
        }
      }
    this.http.post('http://localhost:1234/client/createshipment', shipment_info).subscribe( (response: any) => { 
      if (response)
      {
        this.return_ref_num = response.ref;
        console.log("Shipment added!", response.ref);
      }
      else
        throw new Error ("Cannot add shipment!");
      this.have_quote = false;
    });
  }
}
