import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-get-shipment',
  templateUrl: './get-shipment.component.html',
  styleUrls: ['./get-shipment.component.scss']
})
export class GetShipmentComponent implements OnInit {

  constructor(private http: HttpClient) { }

  sender_name: string;
  sender_email: string;
  sender_phone: number;
  sender_address: string;
  sender_locality: string;
  sender_postal_code: number;
  sender_country: string;

  receiver_name: string;
  receiver_email: string;
  receiver_phone: number;
  receiver_address: string;
  receiver_locality: string;
  receiver_postal_code: number;
  receiver_country: string;

  length: number;
  width: number;
  height: number;
  weight: number;

  //additional parameters
  reference_number: number;
  no_shipment: boolean = true;
  no_action_yet:boolean = true;
  delete_successful: boolean = false;

  getShipment() {
    this.http.get(`http://localhost:1234/client/getshipment/ref/${this.reference_number}`).subscribe( (response : any) => { 
      if (!response.data.ref)
      {
        this.emptyAllParams();
        this.no_shipment = true;
      }
      else
      {
        ({
          origin: {
            contact: {
              name: this.sender_name,
              email: this.sender_email,
              phone: this.sender_phone
            },
            address: {
              country_code: this.sender_country,
              locality: this.sender_locality,
              postal_code: this.sender_postal_code,
              address_line1: this.sender_address
            }
          },
          destination: {
            contact: {
              name: this.receiver_name,
              email: this.receiver_email,
              phone: this.receiver_phone
            },
            address: {
              country_code: this.receiver_country,
              locality: this.receiver_locality,
              postal_code: this.receiver_postal_code,
              address_line1: this.receiver_address
            }
          },
          package: {
            dimensions: {
              height: this.height,
              width: this.width,
              length: this.length,
            },
            grossWeight: {
              amount: this.weight,
            }
          }
        } = response.data);
      }

    });
    this.delete_successful = false;
    this.no_shipment = false;
    this.no_action_yet = false;
  }

  deleteShipment() {
    this.http.post('http://localhost:1234/client/deleteshipment', {ref: this.reference_number}).subscribe((response: any) => {
      if (response.data[0].status == "OK")
      {
        this.emptyAllParams();
        this.no_shipment = true;
        this.delete_successful = true;
        this.no_action_yet = true;
      }
    })
  }

  emptyAllParams() {
    this.sender_name = undefined;
    this.sender_name = undefined;
    this.sender_email = undefined;
    this.sender_phone = undefined;
    this.sender_address = undefined;
    this.sender_locality = undefined;
    this.sender_postal_code = undefined;
    this.sender_country = undefined;

    this.receiver_name = undefined;
    this.receiver_email = undefined;
    this.receiver_phone = undefined;
    this.receiver_address = undefined;
    this.receiver_locality = undefined;
    this.receiver_postal_code = undefined;
    this.receiver_country = undefined;

    this.length = undefined;
    this.width = undefined;
    this.height = undefined;
    this.weight = undefined;
  }

  ngOnInit() {
  }

}
