const { MongoClient } = require('mongodb');

const url = 'mongodb://localhost:27017/mydb';


function findQuoteInDatabase(quoteID, weight, origin, dest) {
  if (weight <= 0) throw new Error('Invalid weight!');
  return new Promise((resolve, reject) => {
    MongoClient.connect(url)
      .then((db) => {
        const dbo = db.db('mydb');
        dbo.collection('rate').findOne({ weight: { $gte: weight }, from: origin, to: dest })
          .then((data) => {
            db.close();
            resolve({ data: [{ id: quoteID, price: data.price }] });
          });
      })
      .catch((err) => {
        throw err;
      });
  });
}


function addShipmentToDatabase(shipmentData) {
  const { cost } = shipmentData.data;
  const referenceNumber = Math.floor(Math.random() * 10000000000);
  const timeCreated = Date();
  const shipmentDataWithReferenceNum = shipmentData;
  shipmentDataWithReferenceNum.data.ref = referenceNumber;
  return new Promise((resolve, reject) => {
    MongoClient.connect(url)
      .then((db) => {
        const dbo = db.db('mydb');
        dbo.collection('shipment').insertOne(shipmentDataWithReferenceNum)
          .then((result) => {
            db.close();
            if (!result) reject(new Error('Cannot add to database'));
            resolve({ ref: referenceNumber, created_at: timeCreated, cost });
          });
      })
      .catch((err) => {
        throw err;
      });
  });
}

function getShipmentFromDatabase(refNum) {
  return new Promise((resolve, reject) => {
    MongoClient.connect(url)
      .then((db) => {
        const dbo = db.db('mydb');
        dbo.collection('shipment').findOne({ 'data.ref': refNum })
          .then((result) => {
            db.close();
            if (!result) reject(new Error('No shipment found'));
            resolve(result);
          });
      })
      .catch((err) => {
        throw err;
      });
  });
}

function deleteShipmentFromDatabase(refNum) {
  return new Promise((resolve, reject) => {
    MongoClient.connect(url)
      .then((db) => {
        const dbo = db.db('mydb');
        dbo.collection('shipment').deleteOne({ 'data.ref': refNum })
          .then((result) => {
            db.close();
            if (result.deletedCount === 0) reject(new Error('Cannot delete shipment'));
            resolve({ data: [{ status: 'OK', message: 'shipment has been deleted' }] });
          });
      })
      .catch((err) => {
        throw err;
      });
  });
}

module.exports.findQuoteInDatabase = findQuoteInDatabase;
module.exports.addShipmentToDatabase = addShipmentToDatabase;
module.exports.getShipmentFromDatabase = getShipmentFromDatabase;
module.exports.deleteShipmentFromDatabase = deleteShipmentFromDatabase;
