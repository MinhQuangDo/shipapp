const { findQuoteInDatabase } = require('./databaseQuery.js');
const { addShipmentToDatabase } = require('./databaseQuery.js');
const { getShipmentFromDatabase } = require('./databaseQuery.js');
const { deleteShipmentFromDatabase } = require('./databaseQuery.js');

const shipment_info1 = {
  data: {
    quote: {
      id: Math.random() * 100,
    },
    origin: {
      contact: {
        name: 'Minh Do',
        email: 'MinhD10@fsoft.com.vn',
        phone: '0923344723',
      },
      address: {
        country_code: 'FR',
        locality: 'Hanoi',
        postal_code: 100000,
        address_line1: '3/23 Doan Ket Land, Kham Thien Str',
      },
    },
    destination: {
      contact: {
        name: 'Ha Tran',
        email: 'nganha.th97@gmail.com',
        phone: '0912344556',
      },
      address: {
        country_code: 'FR',
        locality: 'Granville',
        postal_code: 43023,
        address_line1: '100 W College Str',
      },
    },
    package: {
      dimensions: {
        height: 10,
        width: 20,
        length: 30,
        unit: 'cm',
      },
      grossWeight: {
        amount: 7,
        unit: 'kg',
      },
    },
    cost: 47.49,
  },
};

const shipment_info2 = {
  data: {
    quote: {
      id: Math.random() * 100,
    },
    origin: {
      contact: {
        name: 'Minh Do',
        email: 'MinhD10@fsoft.com.vn',
        phone: '0923344723',
      },
      address: {
        country_code: 'FR',
        locality: 'Hanoi',
        postal_code: 100000,
        address_line1: '3/23 Doan Ket Land, Kham Thien Str',
      },
    },
    destination: {
      contact: {
        name: 'Quang Nguyen',
        email: 'nguyen_q1@denison.edu',
        phone: '0912344556',
      },
      address: {
        country_code: 'FR',
        locality: 'Granville',
        postal_code: 43023,
        address_line1: '100 W College Str',
      },
    },
    package: {
      dimensions: {
        height: 20,
        width: 30,
        length: 70,
        unit: 'cm',
      },
      grossWeight: {
        amount: 9,
        unit: 'kg',
      },
    },
    cost: 58.83,
  },
};

const shipment_info3 = {
    data: {
      quote: {
        id: Math.random() * 100,
      },
      origin: {
        contact: {
          name: 'Minh Do',
          email: 'MinhD10@fsoft.com.vn',
          phone: '0923344723',
        },
        address: {
          country_code: 'FR',
          locality: 'Hanoi',
          postal_code: 100000,
          address_line1: '3/23 Doan Ket Land, Kham Thien Str',
        },
      },
      destination: {
        contact: {
          name: 'Quang Nguyen',
          email: 'nguyen_q1@denison.edu',
          phone: '0912344556',
        },
        address: {
          country_code: 'FR',
          locality: 'Granville',
          postal_code: 43023,
          address_line1: '100 W College Str',
        },
      },
      package: {
        dimensions: {
          height: 20,
          width: 30,
          length: 70,
          unit: 'cm',
        },
        grossWeight: {
          amount: 9,
          unit: 'kg',
        },
      },
      cost: 58.83,
    },
  };


describe('test find quote in rate collection', () => {
  test('should exist', () => {
    expect(findQuoteInDatabase).toBeDefined();
  });

  test('should return $12.43 for <= 250g', () => findQuoteInDatabase(1, 250, 'FR', 'FR').then((res) => {
    expect(res.data[0].price).toBe(12.43);
  }));

  test('should return $20.77 for 1001g', () => findQuoteInDatabase(1, 1001, 'FR', 'FR').then((res) => {
    expect(res.data[0].price).toBe(20.77);
  }));

  test('should return $36.77 for 4999g', () => findQuoteInDatabase(1, 4999, 'FR', 'FR').then((res) => {
    expect(res.data[0].price).toBe(36.77);
  }));

  test('should return $36.77 for 5000g', () => findQuoteInDatabase(1, 5000, 'FR', 'FR').then((res) => {
    expect(res.data[0].price).toBe(36.77);
  }));

  test('should return $88.19 for 11000g', () => findQuoteInDatabase(1, 11000, 'FR', 'FR').then((res) => {
    expect(res.data[0].price).toBe(88.19);
  }));

  test('should return $100 for >= 15000g', () => findQuoteInDatabase(1, 20000, 'FR', 'FR').then((res) => {
    expect(res.data[0].price).toBe(100);
  }));

  test('should throw error for 0g', () => {
    expect(() => findQuoteInDatabase(1, 0, 'FR', 'FR')).toThrow();
  });

  test('should throw error for negative g', () => {
    expect(() => findQuoteInDatabase(1, -1, 'FR', 'FR')).toThrow();
  });

  test('cannot find origin, throw error', () => {
    expect(() => findQuoteInDatabase(1, -1, 'HN', 'FR')).toThrow();
  });

  test('cannot find destination, throw error', () => {
    expect(() => findQuoteInDatabase(1, -1, 'FR', 'HCM')).toThrow();
  });

});



describe('add shipment in database', () => {
  test('should exist', () => {
    expect(addShipmentToDatabase).toBeDefined();
  });

  test('insert successful, data matched', () => addShipmentToDatabase(shipment_info1).then((res) => {
    expect(res).toHaveProperty('ref');
  }).catch((err) => {
      expect(err).toEqual(Error());
  }));
});



describe('get shipment from database', () => {
  test('should exist', () => {
    expect(getShipmentFromDatabase).toBeDefined();
  });

  test('insert successful, then get shipment info based on reference number', () => {
    addShipmentToDatabase(shipment_info2).then((response) => {
      const ref_num = response.ref;
      return getShipmentFromDatabase(ref_num).then((res) => {
        expect(res.data.ref).toBe(ref_num);
      });
    });
  });

  test('get shipment with no reference number', () => getShipmentFromDatabase(NaN).catch((err) => {
    expect(err).toEqual(Error('No shipment found'));
  }));


  test('get shipment with wrong reference number', () => getShipmentFromDatabase(100).catch((err) => {
    expect(err).toEqual(Error('No shipment found'));
  }));

  test('get shipment with negative reference number', () => getShipmentFromDatabase(-1).catch((err) => {
    expect(err).toEqual(Error('No shipment found'));
  }));
});

describe('delete shipment from database', () => {
  test('should exist', () => {
    expect(deleteShipmentFromDatabase).toBeDefined();
  });

  test('insert successful, then delete shipment info based on reference number', () => {
    addShipmentToDatabase(shipment_info3).then((response) => {
      const ref_num = response.ref;
      return deleteShipmentFromDatabase(ref_num).then((res) => {
        expect(res.data[0].status).toBe('OK');
      });
    });
  });

  test('delete an object with wrong reference number', () => deleteShipmentFromDatabase(123456).catch((err) => {
    expect(err).toEqual(Error('Cannot delete shipment'));
  }));
});
